package ru.bmstu.iu7.compiler;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import ru.bmstu.iu7.compiler.grammar.MinijavaBaseVisitor;
import ru.bmstu.iu7.compiler.grammar.MinijavaParser;

import java.util.*;

public class InitializationBeforeUseChecker extends MinijavaBaseVisitor<Set<Symbol>> {
    MinijavaParser parser;
    final Map<String, Klass> klasses;
    //Stack<ru.bmstu.iu7.compiler.Scope> scopes = new Stack<ru.bmstu.iu7.compiler.Scope>();
    ParseTreeProperty<Scope> scopes;
    Scope currentScope = null;
    //Set<ru.bmstu.iu7.compiler.Symbol> initializedInIf = null;
    //Set<ru.bmstu.iu7.compiler.Symbol> initializedInElse = null;

    public InitializationBeforeUseChecker(final Map<String, Klass> klasses, ParseTreeProperty<Scope> scopes, MinijavaParser parser){
        this.scopes=scopes;
        this.klasses=klasses;
        this.parser=parser;
    }
    @Override public Set<Symbol> visitClassDeclaration(@NotNull MinijavaParser.ClassDeclarationContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        exitScope();
        return null; 
    }

    @Override public Set<Symbol> visitMethodDeclaration(@NotNull MinijavaParser.MethodDeclarationContext ctx) {
        enterScope(ctx);
        visitChildren(ctx);
        exitScope();
        return null; 
    }
    //@Override public void enterIfElseStatement(@NotNull ru.bmstu.iu7.MinijavaParser.IfElseStatementContext ctx) {}
    @Override public Set<Symbol> visitIfElseStatement(@NotNull MinijavaParser.IfElseStatementContext ctx) {
        visit(ctx.expression());
        Set<Symbol> initializedVariables = visit(ctx.ifBlock());
        initializedVariables.retainAll(visit(ctx.elseBlock()));
        for(Symbol sym : initializedVariables){
            currentScope.initialize(sym);
        }
        return initializedVariables;
    }
    @Override public Set<Symbol> visitIfBlock(@NotNull MinijavaParser.IfBlockContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        Set<Symbol> ifInit = currentScope.getInitializedVariables();
        exitScope();
        return ifInit;
    }
    @Override public Set<Symbol> visitElseBlock(@NotNull MinijavaParser.ElseBlockContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        Set<Symbol> elseInit = currentScope.getInitializedVariables();
        exitScope();
        return elseInit;
    }
    @Override public Set<Symbol> visitWhileStatement(@NotNull MinijavaParser.WhileStatementContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        exitScope();
        return null;
    }
    @Override public Set<Symbol> visitVariableAssignmentStatement(@NotNull MinijavaParser.VariableAssignmentStatementContext ctx) {
        Set<Symbol> sym = visitChildren(ctx);
        currentScope.initialize(currentScope.lookup(ctx.Identifier().getText()));
        return sym;
    }
    @Override public Set<Symbol> visitIdentifierExpression(@NotNull MinijavaParser.IdentifierExpressionContext ctx) {
        String identifier = ctx.Identifier().getText();
        if(!currentScope.hasBeenInitialized(identifier)){
            ErrorPrinter.printVariableMayNotHaveBeenInitializedError(parser, ctx.Identifier().getSymbol(), identifier);
        }
        return visitChildren(ctx);
    }
    public void enterScope(ParserRuleContext ctx){
        currentScope = scopes.get(ctx);
    }
    private void exitScope(){
        currentScope = currentScope.getEnclosingScope();
    }
}
