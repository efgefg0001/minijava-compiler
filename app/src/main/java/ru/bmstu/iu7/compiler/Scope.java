package ru.bmstu.iu7.compiler;

import java.util.Set;

/**
 * An interface for defining scopes and scoping rules for Minijava.
 */
public interface Scope {
    /**
     * @return the name of this scope.
     */
    String getScopeName();

    /**
     * Where to look next for symbols;
     *
     * @return the scope that contains this scope.
     */
    Scope getEnclosingScope();

    /**
     * Define a symbol in the current scope
     *
     * @param sym A symbol to be defined in this scope.
     */
    void define(Symbol sym);

    /**
     * Adds symbol to a collection of initialized variables in this scope.
     *
     * @param sym The symbol to be initialized
     */
    void initialize(Symbol sym);

    /**
     * Searches for the symbol in the scope.  If this scope does not contain
     * a symbol with the symbolName 'name',
     * return getEnclosingScope==null ? null : getEnclosingScope().lookup(name);
     *
     * @param name the name of the symbol that you would like to lookup
     * @return the symbol that is being referenced, or null if it is
     * not in the symbol-table
     */
    Symbol lookup(String name);

    /**
     * Searches for the symbol in the current scope.
     * If this scope does not contain a symbol with the symbolName name,
     * return null;
     *
     * @param name the name of the symbol that you would like to lookup
     * @return the symbol that is being referenced,
     * or null if it is not in the current scope.
     */
    Symbol lookupLocally(String name);

    /**
     * @param name the name of the symbol that may
     *             or may not have been initialized
     * @return true if the symbol with the given name has been
     * initialized and false otherwise.
     */
    boolean hasBeenInitialized(String name);

    /**
     * @return a set of all of the initialized variables in the this scope.
     */
    Set<Symbol> getInitializedVariables();

    /**
     * @param scope A ru.bmstu.iu7.compiler.Scope for which you want to know
     *              the ru.bmstu.iu7.compiler.Klass that contains it.
     * @return the ru.bmstu.iu7.compiler.Klass that contains scope.
     * If a ru.bmstu.iu7.compiler.Method m is overwritten in a subclass,
     * getEnclosingKlass(m) returns the subclass
     */
    static Klass getEnclosingKlass(Scope scope) {
        while (!(scope instanceof Klass)) {
            scope = scope.getEnclosingScope();
        }
        return (Klass) scope;//The outermost scope will always be a class.
    }

    /**
     * @param scope A scope for which you want to know the ru.bmstu.iu7.compiler.Method that contains it.
     * @return the ru.bmstu.iu7.compiler.Method that contains scope.
     */
    static Method getEnclosingMethod(Scope scope) {
        while (!(scope instanceof Method)) {
            scope = scope.getEnclosingScope();
        }
        return (Method) scope;
    }
}
